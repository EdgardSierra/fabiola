'use client'

import React, { useEffect, useState } from 'react'
import { useRouter } from 'next/navigation'
import confetti from 'canvas-confetti'
import toast, { Toaster } from 'react-hot-toast';
import Head from 'next/head'



const Gifts = () => {

    const [gitOneOpem, setgitOneOpem] = useState(false)
    const [inputValue, setInputValue] = useState('');
    const [inputValue2, setInputValue2] = useState('');
    const router = useRouter()

    const notify = (text) => toast.success(text);
    const notifyErr = (text) => toast.error(text);


    useEffect(() => {


        setTimeout(() => {
            confetti({
                particleCount: 200,
                spread: 70,
                origin: { y: 0.5 }
            });

        }, 1000);
    }, [])


    const confettiFW = () => {

        function randomInRange(min, max) {
            return Math.random() * (max - min) + min;
        }

        confetti({
            angle: randomInRange(55, 125),
            spread: randomInRange(50, 70),
            particleCount: randomInRange(50, 100),
            origin: { y: 0.6 }
        });

    }

    const submitInput1 = () => {

        const key = 'FABIOLINA30'

        if (inputValue !== key) {
            return notifyErr("Escribí bien, Tal cual como está en el codigo")

        }

        if (inputValue === key) {
            notify("ESOOOO COMO ANDAS")
            setTimeout(() => {

                router.push('/openGiftOne');
            }, 3000);
        } else {
            notifyErr('Es que tenes que escribir algo')
        }
        setInputValue('')
    }

    const submitInput2 = () => {

        const key = 'qfqsdf'

        if (inputValue2 !== key) {
            return notifyErr("Que andas haciendo aquí, si este regalo aun no se ha liberado")

        }

        if (inputValue2 === key) {
            notify("ESOOOOO!")
            setTimeout(() => {

                router.push('/openGiftOne');
            }, 1000);
        } else {
            notifyErr('Es que tenes que escribir algo')
        }
        setInputValue2('')
    }

    const regaloUno = () => {
        setgitOneOpem(!gitOneOpem)
    }


    return (
        <>
            <Head>

            </Head>
            <main className="flex min-h-screen flex-col items-center justify-between  bg-gradient-to-br from-[#fc00ff] to-[#00dbde] p-3">
                <Toaster />

                <div className='m-auto bg-white p-5 max-w-4xl rounded-xl text-sm leading-6 text-center '>
                 
                    <p className='font-medium text-slate-600 mb-5 leading-8'>
                        Como es de esperar, un ser humano como usted debe de estar muy ansiosa por saber QUÉ ES LO QUE ESTA PASANDO (En caso que haya entrado a la pagina antes, desde que se la mandaron por twitter), lo sé, lo siento, pero todo esto era parte de este dia tan importante. 

                        Eileen Fabiola Velasquez Cruz, FELIZ CUMPLEAÑOS. Dios nos ha dado la dicha de poder conocerla y que esa luz que brilla desde lo mas profundo de su corazon se propague por todo ser humano que se le cruza en el camino, 
                        es importante decir que cumplir años para usted es un tema muy importante y de interes, por la fecha. Existen fechas especiales para usted, y esta es una de ellas. Le doy la bienvenida a una nueva etapa de su vida, 
                        donde seguirá equivocandose, pero se volverá más sabia, seguirá amando, pero con mas cautela, se seguirá cuidando y nutriendo su alma y corazon, con mas intensidad, no le emociona eso?
                            Dejar atras los 20s puede ser hasta triste en ciertos punto, pero le aseguro que en los 30s usted la VA A ROMPER. Siempre matenga esa autenticidad que la hacen Eileena Fabiolina, nunca deje de ser luz. Siempre sea su prioridad, 
                            nunca deje de soñar alto. Eileen, la vida la va a recompensar, se lo aseguro. Nunca se rinda.</p>

                            <p className='font-medium text-slate-600 mb-5 leading-8'>
                        
                        LLEGAMOS AL DIA DE SU CUMPLEAÑOS y se que la parte de los regalos es una de sus favoritas, tanto por lo que da cuando es de tu parte, quiero entregarle un poco de lo que usted ha dado al mundo, y con ello felicitar a la vida por medio de usted por tenerla un año mas con nosotros.
                        Antes de decirle qué es el regalo, quiero que esto lo tome como un proyecto personal, paciencia, esfuerzo, constancia y enfoque son elementos que necesitarás para su regalo y para su vida. 
                        como todo en la vida, recordarle que lo bueno lleva tiempo. Aquí le dejo su primer problema de los 30, son 2316 problemas a resolver, si, 2,316 (dos mil trescientos dieciséis), tomese su tiempo
                        para saber como resolverlo y asi como en la vida, todo problema al final del dia, tiene su solución. Pero ese proceso de nunca rendirse, la llevará hasta donde usted desee.
                        
                         Tomelo como un proyecto personal para seguir nutriendo su corazon y alma. </p>



                         <p className='font-semibold text-xl text-slate-600 mb-5 leading-8 uppercase '> Ya podes abrir tu regalo </p>

                       
                        <span onClick={confettiFW} className='text-white rounded-xl leading-8 font-bold cursor-pointer bg-purple-600 p-3 my-10'> TOQUE! TOQUE! 🎉 </span>

                    
                    <div className='mt-10'>
                        <div className='grid grid-cols-2 m-auto gap-3'>
                            <div  className='bg-purple-500 w-full  rounded-xl p-5 m-auto flex flex-col'> <span className='m-auto text-xl text-white font-semibold'>Un regalo para las 2</span><span className='m-auto  text-white  uppercase font-light text-sm'>Fecha: 18 mayo 23</span></div>
                            
                            <div  className='bg-purple-500 w-full  rounded-xl p-5 m-auto flex flex-col'> <span className='m-auto text-xl text-white font-semibold'>Un regalo para vos</span><span className='m-auto  text-white  uppercase font-light text-sm'>Fecha: <span>AUN NO DISPONIBLE</span></span></div>
                        </div>
                    </div>

                    <div className='grid grid-cols-2 m-auto gap-3'>

                        <div className='mt-5'>
                            <input
                                type='text'
                                value={inputValue}
                                onChange={(event) => setInputValue(event.target.value)}
                                className='text-black w-full py-2.5 focus:border-purple-600 focus:border-2 border-2 border-purple-600 leading-6 text-sm p-3 rounded-xl outline-none'
                                placeholder='Ingrese el codigo del regalo 1'

                            />
                        <button className='mt-5 w-full border py-2.5 focus:border-purple-600 text-white focus:border-2 bg-purple-600 leading-6 text-sm p-3 rounded-xl outline-none' onClick={submitInput1}  > Canjear Regalo 1 </button>
                        </div>
                        <div className='mt-5'>
                            <input
                                type='text'
                                value={inputValue2}
                                onChange={(event) => setInputValue2(event.target.value)}
                                className='text-black w-full py-2.5 focus:border-purple-600 focus:border-2 border-2 border-purple-600 leading-6 text-sm p-3 rounded-xl outline-none'
                                placeholder='Ingrese el codigo del regalo 2'
                                disabled

                            />
                        <button className='mt-5 w-full border py-2.5 focus:border-purple-600 text-white focus:border-2 bg-purple-600 leading-6 text-sm p-3 rounded-xl outline-none' onClick={submitInput2}  > Canjear Regalo 2 </button>

                        </div>
                    </div>



                </div>
            </main>
        </>
    )
}

export default Gifts