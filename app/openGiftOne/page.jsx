import React from 'react'
import img1 from '../../public/img/nina1.png'
import Image from 'next/image'

const OpenGiftOne = () => {
    return (
        <div className="flex min-h-screen flex-col items-center justify-between  bg-gradient-to-br from-[#fc00ff] to-[#00dbde] p-3">


            <div className='m-auto bg-white py-10 px-3 max-w-3xl rounded-xl text-sm leading-6 text-center '>
                <div className='mb-5 text-slate-700  text-left'>Eileencita,</div>
                <div className='font-medium text-slate-600 text-justify'>


                    <p>
                        Te cuento que hoy estás cumpliendo 30 años y antes de felicitarte por tu cumpleaños, quiero felicitarte por seguir siendo una niña tan amorosa y noble. Seguís siendo la misma niña que has sido siempre, no has perdido nada de tu esencia tan única e inigualable. Has conocido varios países y también has probado la comida deliciosa de ellos.

                    </p>

                    <p className='my-5'>

                        Quiero que sepás también lo importante que te has convertido en la vida de muchas personas. Es en serio, no tenés idea de lo bienvenida que sos en el hogar de todos los lugares a los que llegás. Has impactado la vida de las personas que has conocido, muchas de ellas han cambiado gracias a vos, para bien.

                        Pero es que decime, ¿cómo no te van a amar con esta sonrisa tan linda que tenés?

                    </p>
                    <div className='flex flex-col'>

                        <Image alt='eileen' width={200} height={200} className='mx-auto mt-5 rounded-xl  border-2 border-pink-400' src={'https://res.cloudinary.com/dente/image/upload/v1684294784/samples/nina1_isti48.png'} />
                        <label className='mb-5 mt-2 font-semibold text-center'>Sonrisa de Eileencita</label>
                    </div>
                    <div>

                        <p>

                            Y así como has mantenido tu esencia, no has dejado de ser esa niña traviesa, juguetona y curiosa que desde pequeña ha traído una sed insaciable de conocer el mundo. Tu sonrisa se ha convertido en una de las más hermosas y, por supuesto, el mundo ha tenido la oportunidad de disfrutar de ella.

                            No dejás tampoco de sorprenderte por esas cosas que descubrís por tu propia curiosidad, porque siempre te gusta aprender cosas nuevas, que te enseñen cosas interesantes. Esa emoción de sorpresa que se dibuja en tu cara al querer conocer más y más del mundo. No tenés límites, niña, y nunca los tendrás conociendo el mundo de la manera en que lo haces. Sos esa niña que irradia alegría en las calles, así como en cada paso que dabas mientras ibas a la escuela junto con tus hermanas y regresabas con el pelo como leona.

                            Sigo sin entender hasta el día de hoy cómo es posible que a una niña como vos, en los cumpleaños le siga emocionando y gustando el PASTEL. Para tu suerte, ahora podés comprarte todos los pasteles que queras mi niña, y sí... El pastel tradicional con relleno de piña que te gusta, lo has comprado y comido sin necesidad de que sea cumpleaños.
                        </p>
                        <div className='flex flex-col'>

                            <Image alt='eileen' width={200} height={200} className='mx-auto mt-5 rounded-xl  border-2 border-pink-400' src={'https://res.cloudinary.com/dente/image/upload/v1684294784/samples/nina4_pd0elv.png'} />
                            <div className='mb-5 mt-2 font-semibold text-center'>Objetivo Militar de Eileencita: <strong>PASTEL</strong></div>
                        </div>

                        <p className='my-5'>

                            Te cuento que hay mucha gente que hoy en día te ama con su vida y daría su vida por vos. Has construido relaciones estrechas con muchas personas, te enamoraste, te desilusionaste, lloraste, te equivocaste muchas veces, caíste, te levantaste y volviste a caer, pero ¿sabés? Seguís de pie y me impresiona que una niña como vos sea tan fuerte y valiente. El mundo es poco para lo que una niña como vos merece.
                        </p>

                    </div>
                    <p>
                        Eileencita, siempre voy a destacar tu valor al enfrentar la vida. Quizás aún no lo sepas, pero confiá en la vida, que te dará las herramientas necesarias y poderes infinitos para salir adelante ante cualquier tipo de adversidad que se te presente.

                        Es importante que sepas que tu familia sigue siendo hermosa y sigue dándote ese apoyo y amor interminable e incondicional que siempre te ha dado. Tu familia ha crecido en número, y así ha multiplicado tu amor por ellos. Ya sos tía de dos niñas lindas y traviesas como vos, y de un varoncito muy educado y soñador.
                    </p>
                    <p className='my-5'>

                        Parece mentira, ¿verdad? Pero es lo que la vida te ha preparado para brillar. Nada en el mundo será tan fuerte como para vencerte, mi niña. Sos la niña más fuerte, la más inteligente y la más valiente. Hoy en día, sos una de esas personas que se sienten tu ausencia cuando no estás.
                    </p>
                    <p className='my-5'>
                        Has tomado una pasión por el arte, te gusta mucho la pintura pero principalmente la música. Tu estilo de música es tan divertido como el ska. También te gusta la música clásica porque tu papá te compartió su música, y disfrutas compartir música con tus amigos. Gracias a eso, has conocido a muchas personas.

                        Tenés una perrita hermosa llamada Luna. Es una perrita muy linda y juguetona. Vos eres la vida de Lunita Fabiana, nunca se separan y Lunita Fabiana siempre te llenará de mucho amor.

                    </p>
                    <div className='flex flex-col'>

                        <Image alt='eileen' width={200} height={200} className='mx-auto mt-5 rounded-xl  border-2 border-pink-400' src={'https://res.cloudinary.com/dente/image/upload/v1684294784/samples/nina2_nwvfeg.png'} />
                        <label className='mb-5 mt-2 font-semibold text-center'>Eileencita Sindi: <strong>Sindientes</strong></label>
                    </div>


                    <p className='my-5'>

                        Te recuerdo que hasta el día de hoy te has vuelto más valiente, más fuerte y más independiente. Poco a poco te has convertido en una mujer muy autosuficiente. Espero que eso te brinde mucha alegría y felicidad. No tenés idea de lo inteligente que te has vuelto y ni te imaginas lo hermosa que sos ahora como mujer, un 20/10.

                        Vas a seguir así y nunca te vas a detener, porque Dios tiene un propósito para vos.

                    </p>
                    <label className='my-10 font-bold text-slate-500 '>Las flores son un regalo de cumpleaños para la niña que merece todo lo mejor de la vida.</label>
                    <h1 className='my-10'>Eileen,</h1>
                    <p className='my-5'>
                        Quiero que sepás que en verdad me siento muy orgulloso y feliz por todo lo que has logrado. Tu crecimiento como mujer y ser humano. Estoy seguro de que entrás a esta nueva etapa con una mente muy diferente a lo que eras hace 5 años. Ahora sos una mujer más sabia, más honesta, más real y leal. Una mujer que ha aprendido a manejar y gestionar mejor sus recursos de la mejor manera.
                        Hay grandes cosas para vos en la vida, Fabiola. La determinación que tengas con tus objetivos deberá ser muy consistente y enfocada para que sigas avanzando en la vida y siempre ganando como Belinda. Siempre serás un punto de referencia para las personas, ya sea como buena compañera, amiga, cuñada, nuera, novia, hermana, tía o hija o bien ya sea por tu gentileza, educación, locura, amabilidad, inteligencia, diversión, elocuencia, curiosidad, carácter fuerte pero noble, amorosa, y extrovertida, por tu belleza. De algo sos referencia.
                    </p>

                    <p className='my-10'>
                        Tu manera de amar le da grandeza a tu alma. El alma que pocos conocemos, sin embargo, es lo que te ha convertido hoy en día en una mujer adulta y reponsable con gustos dementes que puede gestionar perfectamente los recursos, tanto monetarios como humanos de una familia hogar. Sos un ser humano increíble. Y me alegra que día a día siempre pensés en mejorar y conocerte más a vos misma, sanar tus heridas de manera individual o con el apoyo de un psicólogo. Aprender a conocerte mejor día a día es mejorar como ser humano. Recordá que no solo te haces un favor a vos misma, sino que estás interconectada con Eileencita, quien te agradece por seguir cuidándola tanto.
                        Espero de todo corazon que en la vida te sucedan cosas hermosas, y cuando ocurran, espero que podás creer que mereces cada una de ellas.
                    </p>
                    <label className='my-10 font-bold text-slate-500 '>Las rosas son un regalo de cumpleaños a la mujer mas importante de mi vida.</label>
                    <h1 className='my-10'>con mucho amor, Edgard.</h1>




                    <div className='flex flex-col'>

                        <Image alt='eileen' width={400} height={400} className='mx-auto mt-5 rounded-xl border-2 border-pink-400 ' src={'https://res.cloudinary.com/dente/image/upload/c_scale,w_600/v1684300986/edgar_33_joj93u.png'} />

                    </div>




                </div>
            </div>
        </div>
    )
}

export default OpenGiftOne