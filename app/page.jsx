'use client'
import Image from 'next/image'
import Link from 'next/link'
import { useState } from 'react';

import { Counter } from './components/Counter';
 




export default function Home() {
 


  const [isCountdownFinished, setIsCountdownFinished] = useState(false);

  const handleCountdownComplete = () => {
    setIsCountdownFinished(true);
  };


  const targetDate = new Date();
  targetDate.setDate(targetDate.getDate() + (4 - targetDate.getDay()));
  targetDate.setHours(10, 00, 0, 0);



  return (
    <main className="flex min-h-screen flex-col items-center justify-between  bg-gradient-to-br from-[#fc00ff] to-[#00dbde] p-3">

      <div className='m-auto bg-white  max-w-4xl rounded-xl text-sm leading-6 text-center '>
        <div></div>
        {/* <p className='font-medium text-slate-600 p-10'>Ya casi estás cerca de cumplir años, y me gustaría jugar con vos algo. Debes de ser muy cuidadosa con las intrucciones (no creo que sean muchas), sin embargo
          no es cualquier cumpleaños, ya vas a cumplir 30, y con esto se viene un sin fin de aventuras nuevas por vivir y un ser humano como vos, merece esto y más.. </p> */}

        
              <h1 className=' font-bold max-w-xl text-purple-700 uppercase p-10 px-10'> Debe de estar pendiente a la hora que se le indica, ya que se habilitará un botón en el cual deberá de seguir un par de instrucciones</h1>

              <h1 className=' font-bold text-purple-700 text-lg  uppercase '> Fecha: 18 Mayo, 23 </h1>
              <h1 className=' font-bold text-purple-700 text-lg  uppercase mb-10'> Hora: 10:00 🕗</h1>

              
            
        <div className='my-10 px-10'>
          <Counter targetDate={targetDate} />
        </div>
         
      </div>
    </main>
  )
}

