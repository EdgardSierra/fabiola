 
import Link from 'next/link';
import React, { useState } from 'react'
import Countdown from 'react-countdown';

export const Counter = ({ targetDate }) => {

    const renderer = ({ days, hours, minutes, seconds, completed }) => {
        if (completed) {
            // Render a completed state
            return <Completionist className="p-10" />
        } else {
            // Render a countdown
            return (
                <table className='m-auto bg-purple-500/10 shadow-lg rounded-xl p-2'>
                    <thead className='p-10'>
                        <tr>
                            <td className='px-2 text-indigo-500'>DÍAS</td>
                            <td className='px-2 text-indigo-500'>HORAS</td>
                            <td className='px-2 text-indigo-500'>MINUTOS</td>
                            <td className='px-2 text-indigo-500'>SEGUNDOS</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td className='text-purple-700 font-bold text-2xl'>{days}</td>
                            <td className='text-purple-700 font-bold text-2xl'>{hours}</td>
                            <td className='text-purple-700 font-bold text-2xl'>{minutes}</td>
                            <td className='text-purple-700 font-bold text-2xl'>{seconds}</td>
                        </tr>
                    </tbody>
                </table>
                )

        }
    };
    const [dateTime, setdateTime] = useState({ days: renderer.days, hours: renderer.hours, minutes: renderer.minutes, seconds: renderer.seconds, completed: renderer.completed })
    const {days, hours, minutes, seconds, completed} = dateTime

    const Completionist = () => (

        <Link href={'/gifts'} className='text-md uppercase font-semibold bg-purple-600 text-white p-3 leading-8 py-5 px-10 tracking-wider w-full rounded-xl shadow-lg shadow-purple-500/80'>FELIZ CUMPLEAÑOS 🎉</Link>

    )
    return (
        <>
        {
            !dateTime.completed ?
            null
            : 
            <h1 className='text-slate-600 font-bold text-md uppercase mb-3'>Cuenta Regresiva</h1>

        }
            <Countdown date={targetDate} renderer={renderer} className='text-xl p-10 text-slate-800' />
        </>
    )
}
