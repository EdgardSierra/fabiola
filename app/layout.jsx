import Head from 'next/head'
import './globals.css'
import { Inter } from 'next/font/google'

const inter = Inter({ subsets: ['latin'] })

export const metadata = {
  title: '🎉✨ Eileenas Birthday ',
  description: 'Cumpleaños de Eileen Fabiola',
}

export default function RootLayout({ children }) {
  return (
    <html lang="es">
      <Head>
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, viewport-fit=cover"></meta>
      
      </Head>
      <body className={inter.className}>{children}</body>
    </html>
  )
}
